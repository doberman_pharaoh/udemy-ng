#!/bin/sh
vboxmanage startvm devserver

read -p "VM ready?? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "Running udemy-ng environment ...";
    sudo mount -t vboxsf drpbx /DATA/DROPBOX
    gnome-terminal --tab --tab-with-profile=devserver
    code

    #watch
    src="/home/devpc/tutorials/udemy-ng/" #don't forget about slash at the path end
    trg="/DATA/DROPBOX/tutorials/udemy-ng/"

    while inotifywait --recursive --event modify,create,delete $src; do
        sudo rsync -avz --exclude="node_modules" $src $trg
    done

    #sudo sshfs -o nonempty,allow_other,IdentityFile=/home/mainpc/.ssh/id_rsa workpc@192.168.56.2:/home/workpc/tutorials/ /LOCALWSRV
    #subl
fi
