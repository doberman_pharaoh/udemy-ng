#!/bin/sh
#src="/DATA/DROPBOX/tutorials/udemy-ng/" #don't forget about slash at the path end
#trg="/LOCALWSRV/udemy-ng/"
src="/home/devpc/tutorials/udemy-ng/" #don't forget about slash at the path end
trg="/DATA/DROPBOX/tutorials/udemy-ng/"

while inotifywait --recursive --event modify,create,delete $src; do
  sudo rsync -avz --exclude="node_modules" $src $trg
done
